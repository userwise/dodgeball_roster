Rails.application.routes.draw do
  get 'sign_in', to: 'sessions#new'
  post 'sign_in', to: 'sessions#create'
  delete 'sign_out', to: 'sessions#destroy'

  resources :players, only: [:index]
  resources :teams, only: [:index, :new, :create, :destroy]

  root 'teams#index'
end
