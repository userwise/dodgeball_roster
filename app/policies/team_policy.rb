class TeamPolicy < ApplicationPolicy
  def create?
    user.admin
  end
end