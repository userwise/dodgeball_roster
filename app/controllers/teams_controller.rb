class TeamsController < ApplicationController
  def index
    @teams = Team.all
  end

  def new
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)
    authorize @team

    if @team.save
      flash[:notice] = "Team saved!"
      redirect_to teams_path
    else
      flash.now[:alert] = "Could not save your team"
      render :new
    end
  end

  def destroy
    @team = Team.find_by_id(params[:id])
    @team.destroy
 
    respond_to do |format|
      format.html {redirect_to teams_path, notice: "#{@team.name} was deleted" }
      format.js
    end
  end

  private

  def team_params
    params.require(:team).permit(:name, :logo)
  end
end
